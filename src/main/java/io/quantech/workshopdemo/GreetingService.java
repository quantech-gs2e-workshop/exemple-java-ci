package io.quantech.workshopdemo;

import org.springframework.stereotype.Service;

@Service
public class GreetingService {
    public String greet() {
        return "Hello, World from BNI";
    }
}
